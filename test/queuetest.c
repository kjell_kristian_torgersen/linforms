#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>

#include "bqueue.h"
#include "gfx.h"

bqueue_t * bq;
bool done = false;
/*void * prod(void * arg)
{
	int i = 0;
	for(;;) {
		
	}
}*/

void * cons(void * arg)
{
	while(!done) {
		void * data = bqueue_dequeue(bq);
		//printf("%i\n", (int)data);
	}
	return NULL;
}

#define N 100

void mycmd(void * arg) {
	printf("Hello World\n");
}

void test_cmd(gfx_t * gfx) {
	gfx_beginInvoke(gfx, mycmd, NULL);
	
}

int main_old(int argc, char const *argv[])
{
	int i = 0;
	bq = bqueue_init();

	pthread_t th[N];
	
	for(int i = 0; i < N; i++) {
		pthread_create(&th[i], NULL, cons, NULL); 
	}

	for(int i = 0; i < 10000; i++) {
		bqueue_enqueue(bq, NULL);
	}

	sleep(1);

	bqueue_done(bq);

	for(int i = 0; i < N; i++) {
		pthread_join(th[i], NULL);
	}

	bqueue_free(bq);

	return 0;
}
