#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#include "gfx.h"

gfx_t * gfx;
ctrl_t * root;
ctrl_t * label;
ctrl_t * progressBar;
ctrl_t * button;
ctrl_t * textbox;
ctrl_t * checkbox[3];

bool done = false;

struct ctrl {
	ctrl_common_t common;
};

void button_clicked(ctrl_t *this, int x, int y, int button) 
{
	printf("Button was clicked\n");
	label->common.backColor = 0xFFAA0000;
	ctrl_textedit_clear(textbox);
}

void button_released(ctrl_t *this, int x, int y, int button) 
{
	printf("Button was clicked\n");
	label->common.backColor = 0xFFAAAAAA;
}

void button_mousemotion(ctrl_t *this, int x, int y) 
{
	if(ctrl_inside(this, x, y)) {
		this->common.backColor = 0xFFFFFFFF;
	} else {
		this->common.backColor = 0xFFAAAAAA;
	}
}

int progress = 0;

void command(void * arg) {
	ctrl_label_setText(label, arg);
	ctrl_progressBar_setProgress(progressBar, (progress++) % 100);
}

void * mythread(void * arg) {
	while(!done) {
		usleep(100000);
		gfx_beginInvoke(gfx, command, "Tick");
		usleep(100000);
		gfx_beginInvoke(gfx, command, "Tack");
	}
	return NULL;
}

int main(int argc, char const *argv[])
{
	pthread_t th;

	gfx = sdlgfx_init(240,120);

	/*root = ctrl_container_init(false);
	label = ctrl_label_init("Hello World");
	progressBar = ctrl_progressBar_init();
	button = ctrl_button_init("Click me");
	textbox = ctrl_textedit_init();
	for(int i = 0; i < 3; i++) {
		char buf[64];
		snprintf(buf, sizeof(buf), "Checkbox %i", i);
		checkbox[i] = ctrl_checkbox_init(buf);
	}

	ctrl_container_append(root, label);
	ctrl_container_append(root, progressBar);
	ctrl_container_append(root, button);
	ctrl_container_append(root, textbox);
	for(int i = 0; i < 3; i++) {
		ctrl_container_append(root, checkbox[i]);
	}
	button->common.events.mousebuttonup = button_released;
	button->common.events.mousebuttondown = button_clicked;
	button->common.events.mousemotion = button_mousemotion;
	button->common.arg = gfx;

	pthread_create(&th, NULL, mythread, gfx);
*/
	root = ctrl_tabs_init();
	ctrl_tabs_addTab(root, "Tab 1",  ctrl_checkbox_init("Checkbox 1"));
	ctrl_tabs_addTab(root, "Tab 2",  ctrl_checkbox_init("Checkbox 2"));
	ctrl_tabs_addTab(root, "Tab 3",  ctrl_checkbox_init("Checkbox 3"));
	gfx_run(gfx, root);
	done = true;
	//pthread_join(th, NULL);
	ctrl_free(root);
	return 0;
}


