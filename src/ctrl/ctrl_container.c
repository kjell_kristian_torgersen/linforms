#include <string.h>
#include <stdlib.h>

#include <dynstruct/parray.h>

#include "ctrl.h"

struct ctrl {
	ctrl_common_t common;
	parray_t * controls;
        rect_t rect;
        ctrl_t * focused;
};

typedef struct state {
        int x0;
        int y0;
        int w;
        int i;
        double size;
        int padding;
        double vsum;
} state_t;

typedef struct eventstate {
        int x;
        int y;
        int button;
        ctrl_t * container;
} eventstate_t;

static void setrecth(void * elem, void * arg) 
{
        ctrl_t * ctrl = elem;
        state_t * state = arg;
        rect_t rect = {state->x0 + state->vsum + state->padding, state->y0 +  state->padding, state->size*ctrl->common.weight - 2*state->padding, state->w - 2 * state->padding};
        ctrl_setrect(ctrl, &rect);
        state->vsum += state->size * ctrl->common.weight;
        state->i++;
}

static void setrectv(void *elem, void *arg)
{
	ctrl_t *ctrl = elem;
	state_t *state = arg;
	rect_t rect = {state->x0 + state->padding, state->y0 + state->vsum + state->padding, state->w - 2 * state->padding, state->size * ctrl->common.weight - 2 * state->padding};
	ctrl_setrect(ctrl, &rect);
	state->vsum += state->size * ctrl->common.weight;
	state->i++;
}

static void recalculateSizes(ctrl_t * this) {
        int n = 0;//parray_size(this->controls);
        for(size_t i = 0; i < parray_size(this->controls); i++) {
                n += ((ctrl_t*)parray_at(this->controls, i))->common.weight;
        }
        if(n != 0) {
                if(this->common.type == CTRL_HCONTAINER) {
                        double size = this->rect.w / (double)n;
                        state_t state = {this->rect.x, this->rect.y, this->rect.h, 0, size, 2, 0.0};
                        parray_fold(this->controls, setrecth, &state);
                } else {
                        double size = this->rect.h / (double)n;
                        state_t state = {this->rect.x, this->rect.y, this->rect.w, 0, size, 2, 0.0};
                        parray_fold(this->controls, setrectv, &state);     
                }
        }
}

static void c_setrect(ctrl_t * this, const rect_t * rect)
{	
        this->rect = *rect;
        recalculateSizes(this);
}

static void c_getrect(ctrl_t * this, rect_t * rect)
{
        *rect = this->rect;
}

static void c_keydown(ctrl_t * this, int x, int y, int key)
{
        //eventstate_t state = {x,y,key,this};
        //parray_fold(this->controls, keydown, &state);
        if(this->focused) {
                ctrl_keydown(this->focused, x, y, key);
        }
}

static void c_keyup(ctrl_t * this, int x, int y, int key)
{
        if(this->focused) {
                ctrl_keyup(this->focused, x, y, key);
        }
}

static void mousemotion(void * elem, void * arg) {
      eventstate_t * state = arg;
      ctrl_mousemotion(elem, state->x, state->y);
}

static void c_mousemotion(ctrl_t * this, int x, int y)
{
        eventstate_t state = {x,y,-1,this};
        parray_fold(this->controls, mousemotion, &state);
}


static void mousebuttondown(void * elem, void * arg) {
        eventstate_t * state = arg;
        rect_t rect;
        ctrl_getrect(elem, &rect);
        if((state->x >= rect.x) && (state->x < rect.x+rect.w)) {
                if((state->y >= rect.y) && (state->y < rect.y+rect.h)) {
                        ctrl_mousebuttondown(elem, state->x, state->y, state->button);
                        state->container->focused = elem;        
                }        
        }
}

static void c_mousebuttondown(ctrl_t * this, int x, int y, int button)
{
        eventstate_t state = {x,y,button,this};
        parray_fold(this->controls, mousebuttondown, &state);
}

static void mousebuttonup(void * elem, void * arg) 
{
        eventstate_t * state = arg;
        rect_t rect;
        ctrl_getrect(elem, &rect);
        if((state->x >= rect.x) && (state->x < rect.x+rect.w)) {
                if((state->y >= rect.y) && (state->y < rect.y+rect.h)) {
                        ctrl_mousebuttonup(elem, state->x, state->y, state->button);        
                }        
        }
}

static void c_mousebuttonup(ctrl_t * this, int x, int y, int button)
{
        eventstate_t state = {x,y,button,this};
        parray_fold(this->controls, mousebuttonup, &state);
}

static void c_update(ctrl_t * this, gfx_t * gfx)
{
        parray_fold(this->controls, (void (*)(void *,void*))ctrl_update, gfx);
}

static void c_free(ctrl_t * this)
{
	parray_free(this->controls);
	free(this);
}

static ctrl_vtable_t vtable = {c_setrect, c_getrect, c_keydown, c_keyup, c_mousemotion, c_mousebuttondown, c_mousebuttonup, c_update, c_free};

void ctrl_container_append(ctrl_t * this, ctrl_t * ctrl) 
{
        parray_append(this->controls, ctrl);
        recalculateSizes(this);
}

void ctrl_container_remove(ctrl_t * this, ctrl_t * ctrl) 
{
        parray_remove(this->controls, ctrl);
        recalculateSizes(this);
}

ctrl_t * ctrl_container_init(bool horizontal) 
{
	ctrl_t * this = malloc(sizeof(ctrl_t));
	if(this) {
                ctrl_common_init(&this->common, &vtable, horizontal ? CTRL_HCONTAINER : CTRL_VCONTAINER);
                this->controls = parray_init((void (*)(void *))ctrl_free);
                this->focused = NULL;
	}
	return this;
}