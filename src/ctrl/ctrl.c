#include <stddef.h>
#include <string.h>
#include <stdbool.h>

#include "ctrl.h"

struct ctrl {
	ctrl_common_t common;
};

bool ctrl_inside(ctrl_t * this, int x, int y) 
{
	rect_t * r = &this->common.rect;
	return (x > r->x) && (x < (r->x + r->w)) && (y > r->y) && (y < (r->y + r->h));
}

void ctrl_setEvents(ctrl_t * this, ctrl_event_t * events, void * arg)
{
	if(events) {
		this->common.events = *events;
	} else {
		memset(&this->common.events, 0, sizeof(ctrl_event_t));
	}
	this->common.arg = arg;
}

void ctrl_common_init(ctrl_common_t * this, ctrl_vtable_t * vtable, ctrl_type_t type) 
{
	this->vtable = vtable;
	memset(&this->events, 0, sizeof(ctrl_event_t));
	this->weight = 1;
	this->arg = NULL;
	this->type = type;
	this->foreColor = 0xFF000000;
	this->backColor = 0xFFAAAAAA;
}
void ctrl_setrect(ctrl_t * this, const rect_t * rect)
{
	this->common.vtable->setrect(this, rect);
}

void ctrl_getrect(ctrl_t * this, rect_t * rect)
{
	this->common.vtable->getrect(this, rect);
}

void ctrl_keydown(ctrl_t * this, int x, int y, int key)
{
	this->common.vtable->keydown(this, x, y, key);
	if(this->common.events.keydown){
		this->common.events.keydown(this, x, y, key);
	}
}

void ctrl_keyup(ctrl_t * this, int x, int y, int key)
{
	this->common.vtable->keyup(this, x, y, key);
	if(this->common.events.keyup){
		this->common.events.keyup(this, x, y, key);
	}
}

void ctrl_mousemotion(ctrl_t * this, int x, int y)
{
	this->common.vtable->mousemotion(this, x, y);
	if(this->common.events.mousemotion){
		this->common.events.mousemotion(this, x, y);
	}
}

void ctrl_mousebuttondown(ctrl_t * this, int x, int y, int button)
{
	this->common.vtable->mousebuttondown(this, x, y, button);
	if(this->common.events.mousebuttondown){
		this->common.events.mousebuttondown(this, x, y, button);
	}
}

void ctrl_mousebuttonup(ctrl_t * this, int x, int y, int button)
{
	this->common.vtable->mousebuttonup(this, x, y, button);
	if(this->common.events.mousebuttonup){
		this->common.events.mousebuttonup(this, x, y, button);
	}
}

void ctrl_update(ctrl_t * this, gfx_t * gfx)
{
	this->common.vtable->update(this, gfx);
}

void ctrl_free(ctrl_t * this)
{
	this->common.vtable->free(this);
}
