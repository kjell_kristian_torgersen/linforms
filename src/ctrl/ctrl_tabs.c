#include <string.h>
#include <stdlib.h>

#include <dynstruct/earray.h>

#include "ctrl.h"
#include "gfx.h"
#include "cgfx.h"

typedef struct tab {
	ctrl_t * button;
	ctrl_t * content;
} tab_t;

struct ctrl {
	ctrl_common_t common;
	ctrl_t * tabContainer;
	earray_t * tabs;
	ctrl_t * currentTab;
};

static void c_setrect(ctrl_t * this, const rect_t * rect)
{	
	this->common.rect = *rect;
	rect_t tab = {0,0,rect->w,30};
	rect_t cont = {0,30,rect->w,rect->h-30};
	ctrl_setrect(this->tabContainer, &tab);
	ctrl_setrect(this->currentTab, &cont);
}

static void c_getrect(ctrl_t * this, rect_t * rect)
{
	*rect = this->common.rect;
}

static void c_keydown(ctrl_t * this, int x, int y, int key)
{
	ctrl_keydown(this->tabContainer, x, y, key);
	ctrl_keydown(this->currentTab, x, y, key);
}

static void c_keyup(ctrl_t * this, int x, int y, int key)
{
	ctrl_keyup(this->tabContainer, x, y, key);
	ctrl_keyup(this->currentTab, x, y, key);
}

static void c_mousemotion(ctrl_t * this, int x, int y)
{
	ctrl_mousemotion(this->tabContainer, x, y);
	ctrl_mousemotion(this->currentTab, x, y);
}

static void c_mousebuttondown(ctrl_t * this, int x, int y, int button)
{
	ctrl_mousebuttondown(this->tabContainer, x, y, button);
	ctrl_mousebuttondown(this->currentTab, x, y, button);
}

static void c_mousebuttonup(ctrl_t * this, int x, int y, int button)
{
	ctrl_mousebuttonup(this->tabContainer, x, y, button);
	ctrl_mousebuttonup(this->currentTab, x, y, button);
}

static void c_update(ctrl_t * this, gfx_t * gfx)
{
	ctrl_update(this->tabContainer, gfx);
	for(int i = 0; i < ea_size(this->tabs); i++) {
		tab_t * tab = ea_at(this->tabs, i);
		ctrl_update(tab->button, gfx);
	}
	if(this->currentTab) {
		ctrl_update(this->currentTab, gfx);
	}
}

static void c_free(ctrl_t * this)
{
	for(int i = 0; i < ea_size(this->tabs); i++) {
		tab_t * tab = ea_at(this->tabs, i);
		//ctrl_free(tab->button);
		ctrl_free(tab->content);
	}
	ctrl_free(this->tabContainer);
	ea_free(this->tabs);
	free(this);
}

static ctrl_vtable_t vtable = {c_setrect, c_getrect, c_keydown, c_keyup, c_mousemotion, c_mousebuttondown, c_mousebuttonup, c_update, c_free};

void tabbutton_clicked(ctrl_t *this, int x, int y, int button) {
	ctrl_t * tabs = this->common.arg;
	for(int i = 0; i < ea_size(tabs->tabs); i++) {
		tab_t * tab = ea_at(tabs->tabs, i);
		if(tab->button == this) {
			tabs->currentTab = tab->content;
			c_setrect(tabs, &tabs->common.rect);
		}
	}
}

void ctrl_tabs_addTab(ctrl_t * this, const char * name, ctrl_t * content) 
{
	ctrl_t * button = ctrl_button_init(name);
	if(ea_size(this->tabs) == 0) {
		this->currentTab = content;
	}
	button->common.arg = this;
	button->common.events.mousebuttondown = tabbutton_clicked;
	tab_t tab = {button, content };
	ea_append(this->tabs, &tab);
	ctrl_container_append(this->tabContainer, button);
}

ctrl_t * ctrl_tabs_init(void) 
{
	ctrl_t * this = malloc(sizeof(ctrl_t));
	if(this) {
		ctrl_common_init(&this->common, &vtable, CTRL_CUSTOM);
		this->tabs = ea_init(sizeof(tab_t));
		this->tabContainer = ctrl_container_init(true);
	}
	return this;
}