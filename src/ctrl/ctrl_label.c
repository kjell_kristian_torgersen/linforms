#include <string.h>
#include <stdlib.h>

#include "ctrl.h"
#include "gfx.h"
#include "cgfx.h"

struct ctrl {
	ctrl_common_t common;
	char * text;
};

static void c_setrect(ctrl_t * this, const rect_t * rect)
{	
	this->common.rect = *rect;
}

static void c_getrect(ctrl_t * this, rect_t * rect)
{
	*rect = this->common.rect;
}

static void c_keydown(ctrl_t * this, int x, int y, int key)
{

}

static void c_keyup(ctrl_t * this, int x, int y, int key)
{

}

static void c_mousemotion(ctrl_t * this, int x, int y)
{

}

static void c_mousebuttondown(ctrl_t * this, int x, int y, int button)
{

}

static void c_mousebuttonup(ctrl_t * this, int x, int y, int button)
{

}

static void c_update(ctrl_t * this, gfx_t * gfx)
{
	gfx_fillRect(gfx, this->common.rect.x, this->common.rect.y, this->common.rect.w, this->common.rect.h, this->common.backColor);
	//cgfx_frame(gfx, this->common.rect.x, this->common.rect.y, this->common.rect.w, this->common.rect.h, 0);
	gfx_drawText(gfx, this->text, this->common.rect.x + this->common.rect.w/2, this->common.rect.y + this->common.rect.h/2, CENTER_W, this->common.foreColor);
}

static void c_free(ctrl_t * this)
{
	free(this->text);
	free(this);
}

static ctrl_vtable_t vtable = {c_setrect, c_getrect, c_keydown, c_keyup, c_mousemotion, c_mousebuttondown, c_mousebuttonup, c_update, c_free};

void ctrl_label_setText(ctrl_t * this, const char * text) 
{
	if(this->text) {
		free(this->text);
	}
	this->text = strdup(text);
}

ctrl_t * ctrl_label_init(const char * text) 
{
	ctrl_t * this = malloc(sizeof(ctrl_t));
	if(this) {
		ctrl_common_init(&this->common, &vtable, CTRL_LABEL);
		this->text = strdup(text);
	}
	return this;
}