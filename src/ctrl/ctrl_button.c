#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "ctrl.h"
#include "gfx.h"
#include "cgfx.h"

struct ctrl {
	ctrl_common_t common;
	char * text;
        bool clicked;
};

static void c_setrect(ctrl_t * this, const rect_t * rect)
{	
        this->common.rect = *rect;
}

static void c_getrect(ctrl_t * this, rect_t * rect)
{
        *rect = this->common.rect;
}

static void c_keydown(ctrl_t * this, int x, int y, int key)
{

}

static void c_keyup(ctrl_t * this, int x, int y, int key)
{

}

static void c_mousemotion(ctrl_t * this, int x, int y)
{
	if(!ctrl_inside(this, x,y)) {
		this->clicked = false;
	}
}

static void c_mousebuttondown(ctrl_t * this, int x, int y, int button)
{
        this->clicked = true;
}

static void c_mousebuttonup(ctrl_t * this, int x, int y, int button)
{
        this->clicked = false;
}

static void c_update(ctrl_t * this, gfx_t * gfx)
{
	gfx_fillRect(gfx, this->common.rect.x, this->common.rect.y, this->common.rect.w, this->common.rect.h, this->common.backColor);
	if(this->clicked) {
                cgfx_frame(gfx, this->common.rect.x, this->common.rect.y, this->common.rect.w, this->common.rect.h, -1);
		gfx_drawText(gfx, this->text, this->common.rect.x + this->common.rect.w / 2-1, this->common.rect.y + this->common.rect.h / 2-1, CENTER_WH, this->common.foreColor);
        } else {
                cgfx_frame(gfx, this->common.rect.x, this->common.rect.y, this->common.rect.w, this->common.rect.h, 1);
		gfx_drawText(gfx, this->text, this->common.rect.x + this->common.rect.w / 2, this->common.rect.y + this->common.rect.h / 2, CENTER_WH, this->common.foreColor);
        }	
}

static void c_free(ctrl_t * this)
{
	free(this->text);
	free(this);
}

static ctrl_vtable_t vtable = {c_setrect, c_getrect, c_keydown, c_keyup, c_mousemotion, c_mousebuttondown, c_mousebuttonup, c_update, c_free};

const char * ctrl_button_text(ctrl_t * this) 
{
	return this->text;
}

ctrl_t * ctrl_button_init(const char * text) 
{
	ctrl_t * this = malloc(sizeof(ctrl_t));
	if(this) {
		ctrl_common_init(&this->common, &vtable, CTRL_BUTTON);
		this->text = strdup(text);
                this->clicked = false;
	}
	return this;
}