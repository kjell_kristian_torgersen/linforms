#include <string.h>
#include <stdlib.h>

#include <dynstruct/darray.h>

#include "ctrl.h"
#include "gfx.h"
#include "cgfx.h"

struct ctrl {
	ctrl_common_t common;
	darray_t * str;
	char * text;
};

static void c_setrect(ctrl_t * this, const rect_t * rect)
{	
	this->common.rect = *rect;
}

static void c_getrect(ctrl_t * this, rect_t * rect)
{
	*rect = this->common.rect;
}

static void c_keydown(ctrl_t * this, int x, int y, int key)
{
	da_add(this->str, &key, 1);
	if(this->text) {
		free(this->text);
	}
	this->text = da_toString(this->str);
}

static void c_keyup(ctrl_t * this, int x, int y, int key)
{

}

static void c_mousemotion(ctrl_t * this, int x, int y)
{

}

static void c_mousebuttondown(ctrl_t * this, int x, int y, int button)
{

}

static void c_mousebuttonup(ctrl_t * this, int x, int y, int button)
{

}

static void c_update(ctrl_t * this, gfx_t * gfx)
{
	
	gfx_fillRect(gfx, this->common.rect.x, this->common.rect.y, this->common.rect.w, this->common.rect.h, this->common.backColor);
	gfx_drawRect(gfx, this->common.rect.x, this->common.rect.y, this->common.rect.w, this->common.rect.h, 0xFF555555);
	if(this->text) {
		gfx_drawText(gfx, this->text, this->common.rect.x + 2, this->common.rect.y + this->common.rect.h / 2, CENTER_H, this->common.foreColor);
	}
	
}

static void c_free(ctrl_t * this)
{
	da_free(this->str);
	if(this->text) {
		free(this->text);
	}
	free(this);
}

static ctrl_vtable_t vtable = {c_setrect, c_getrect, c_keydown, c_keyup, c_mousemotion, c_mousebuttondown, c_mousebuttonup, c_update, c_free};

void ctrl_textedit_appendText(ctrl_t * this, const char * str) 
{
	da_add(this->str, str, strlen(str));
	if(this->text) {
		free(this->text);
	}
	this->text = da_toString(this->str);
}

void ctrl_textedit_clear(ctrl_t * this) 
{
	da_remove_all(this->str);
	if(this->text) {
		free(this->text);
		this->text = NULL;
	}
	
}

ctrl_t * ctrl_textedit_init(void) 
{
	ctrl_t * this = malloc(sizeof(ctrl_t));
	if(this) {
		ctrl_common_init(&this->common, &vtable, CTRL_TEXTEDIT);
		this->common.backColor = 0xFFFFFFFF;
		this->common.foreColor = 0xFF000000;
		this->str = da_init();
		this->text = NULL;
	}
	return this;
}