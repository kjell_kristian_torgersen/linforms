#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "ctrl.h"
#include "gfx.h"
#include "cgfx.h"

struct ctrl {
	ctrl_common_t common;
	int progress;
	int progressMax;
};

static void c_setrect(ctrl_t * this, const rect_t * rect)
{	
	this->common.rect = *rect;
}

static void c_getrect(ctrl_t * this, rect_t * rect)
{
	*rect = this->common.rect;
}

static void c_keydown(ctrl_t * this, int x, int y, int key)
{

}

static void c_keyup(ctrl_t * this, int x, int y, int key)
{

}

static void c_mousemotion(ctrl_t * this, int x, int y)
{

}

static void c_mousebuttondown(ctrl_t * this, int x, int y, int button)
{

}

static void c_mousebuttonup(ctrl_t * this, int x, int y, int button)
{

}

static void c_update(ctrl_t * this, gfx_t * gfx)
{
	char progressStr[64];
	snprintf(progressStr, sizeof(progressStr), "%0.1f %%", 100.0*this->progress/(double)this->progressMax);
	int x = this->common.rect.x;
	int y = this->common.rect.y;
	int w = this->common.rect.w;
	int h = this->common.rect.h;
	double progress = (double)this->progress/(double)this->progressMax;
	gfx_fillRect(gfx, x, y, w, h, this->common.backColor);
	gfx_fillRect(gfx, x, y, (w*progress), h, this->common.foreColor);
	cgfx_frame(gfx, x, y, w, h, -1);
	uint32_t color = progress < 0.5 ? 0xFF000000 : 0xFFFFFFFF;
	gfx_drawText(gfx, progressStr, this->common.rect.x + this->common.rect.w / 2, this->common.rect.y + this->common.rect.h / 2, CENTER_WH, color);
}

static void c_free(ctrl_t * this)
{
	free(this);
}

static ctrl_vtable_t vtable = {c_setrect, c_getrect, c_keydown, c_keyup, c_mousemotion, c_mousebuttondown, c_mousebuttonup, c_update, c_free};

void ctrl_progressBar_setProgress(ctrl_t * this, int progress) 
{
	this->progress = progress;
}

void ctrl_progressBar_setProgressMax(ctrl_t * this, int progressMax) 
{
	this->progressMax = progressMax;
}

ctrl_t * ctrl_progressBar_init(void)
{
	ctrl_t * this = malloc(sizeof(ctrl_t));
	if(this) {
		ctrl_common_init(&this->common, &vtable, CTRL_PROGRESSBAR);
		this->common.foreColor = 0xFF0000AA;
		this->common.backColor = 0xFFAAAAAA;
		this->progress = 0;
		this->progressMax = 100;
	}
	return this;
}