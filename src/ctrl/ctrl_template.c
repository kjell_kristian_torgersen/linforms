#include <string.h>
#include <stdlib.h>

#include "ctrl.h"
#include "gfx.h"
#include "cgfx.h"

struct ctrl {
	ctrl_common_t common;
};

static void c_setrect(ctrl_t * this, const rect_t * rect)
{	
	this->common.rect = *rect;
}

static void c_getrect(ctrl_t * this, rect_t * rect)
{
	*rect = this->common.rect;
}

static void c_keydown(ctrl_t * this, int x, int y, int key)
{

}

static void c_keyup(ctrl_t * this, int x, int y, int key)
{

}

static void c_mousemotion(ctrl_t * this, int x, int y)
{

}

static void c_mousebuttondown(ctrl_t * this, int x, int y, int button)
{

}

static void c_mousebuttonup(ctrl_t * this, int x, int y, int button)
{

}

static void c_update(ctrl_t * this, gfx_t * gfx)
{

}

static void c_free(ctrl_t * this)
{
	free(this);
}

static ctrl_vtable_t vtable = {c_setrect, c_getrect, c_keydown, c_keyup, c_mousemotion, c_mousebuttondown, c_mousebuttonup, c_update, c_free};

ctrl_t * ctrl_template_init(const char * text) 
{
	ctrl_t * this = malloc(sizeof(ctrl_t));
	if(this) {
		ctrl_common_init(&this->common, &vtable, CTRL_CUSTOM);
	}
	return this;
}