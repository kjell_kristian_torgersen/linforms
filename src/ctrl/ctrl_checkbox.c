#include <string.h>
#include <stdlib.h>

#include "ctrl.h"
#include "gfx.h"
#include "cgfx.h"

struct ctrl {
	ctrl_common_t common;
	char * text;
	bool checked;
};

static void c_setrect(ctrl_t * this, const rect_t * rect)
{	
	this->common.rect = *rect;
}

static void c_getrect(ctrl_t * this, rect_t * rect)
{
	*rect = this->common.rect;
}

static void c_keydown(ctrl_t * this, int x, int y, int key)
{

}

static void c_keyup(ctrl_t * this, int x, int y, int key)
{

}

static void c_mousemotion(ctrl_t * this, int x, int y)
{

}

static void c_mousebuttondown(ctrl_t * this, int x, int y, int button)
{
	if(ctrl_inside(this, x, y)) {
		this->checked = !this->checked;
	}
}

static void c_mousebuttonup(ctrl_t * this, int x, int y, int button)
{

}

static void c_update(ctrl_t * this, gfx_t * gfx)
{
	int padd = this->common.rect.h/4;
	int padd2 = this->common.rect.h/10;
	gfx_fillRect(gfx, this->common.rect.x, this->common.rect.y, this->common.rect.w, this->common.rect.h, this->common.backColor);
	gfx_fillRect(gfx, this->common.rect.x+padd, this->common.rect.y+padd, this->common.rect.h-2*padd, this->common.rect.h-2*padd, 0xFFFFFFFF);
	if(this->checked) {
		gfx_fillRect(gfx, this->common.rect.x+padd+padd2, this->common.rect.y+padd+padd2, this->common.rect.h-2*padd-2*padd2, this->common.rect.h-2*padd-2*padd2, 0xff000000);
	}
	cgfx_frame(gfx, this->common.rect.x+padd, this->common.rect.y+padd, this->common.rect.h-2*padd, this->common.rect.h-2*padd, -1);
	
	//cgfx_frame(gfx, this->common.rect.x, this->common.rect.y, this->common.rect.w, this->common.rect.h, 0);
	gfx_drawText(gfx, this->text, this->common.rect.x + 2+this->common.rect.h, this->common.rect.y + this->common.rect.h / 2, CENTER_H, this->common.foreColor);
}

static void c_free(ctrl_t * this)
{
	free(this->text);
	free(this);
}

static ctrl_vtable_t vtable = {c_setrect, c_getrect, c_keydown, c_keyup, c_mousemotion, c_mousebuttondown, c_mousebuttonup, c_update, c_free};

void ctrl_checkbox_setText(ctrl_t * this, const char * text) 
{
	if(this->text) {
		free(this->text);
	}
	this->text = strdup(text);
}

ctrl_t * ctrl_checkbox_init(const char * text) 
{
	ctrl_t * this = malloc(sizeof(ctrl_t));
	if(this) {
		ctrl_common_init(&this->common, &vtable, CTRL_LABEL);
		this->text = strdup(text);
		this->checked = false;
	}
	return this;
}