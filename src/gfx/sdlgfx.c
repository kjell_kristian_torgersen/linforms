#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <dynstruct/bdqueue.h>

#include "gfx.h"

struct gfx {
	gfx_vtable_t * vtable;
	SDL_Window *window;
	SDL_Renderer *renderer;
	int gw, gh;
	TTF_Font* font;
	bdqueue_t * queue;
};

typedef struct cmd {
	void(*cmd)(void *arg);
	void * arg;
} cmd_t;

static int camx(gfx_t * this, int x) {
	//if(this->track) {
	//	return x - this->track->common.pos.x + this->gw / 2;
	//} else {
		return x;
	//}
}

static int camy(gfx_t * this, int y) {
	// if(this->track) {
	// 	return y - this->track->common.pos.y + this->gh / 2;
	// } else {
		return y;
	//}
}

static int icamx(gfx_t * this, int x) {
	// if(this->track) {
	// 	return x + this->track->common.pos.x- this->gw / 2;
	// } else {
		return x;
	//}
}

static int icamy(gfx_t * this, int y) {
	// if(this->track) {
	// 	return y+ this->track->common.pos.y - this->gh / 2;
	// } else {
		return y;
	//}
}

static void g_clear(gfx_t * this, uint32_t color)
{
	//SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, (color >> 16) & 0xFF, (color >> 8) & 0xFF, (color)&0xFF));
	SDL_SetRenderDrawColor(this->renderer,  (color >> 16) & 0xFF, (color >> 8) & 0xFF, (color) & 0xFF, (color >> 24) & 0xFF);
	SDL_RenderClear(this->renderer);
}

static void g_fillRect(gfx_t * this, int x, int y, int w, int h, uint32_t color)
{
	SDL_Rect r = {camx(this, x), camy(this, y), w, h};
	//SDL_FillRect(screenSurface, &r, SDL_MapRGB(screenSurface->format, (color >> 16) & 0xFF, (color >> 8) & 0xFF, (color)&0xFF));
	SDL_SetRenderDrawColor(this->renderer,  (color >> 16) & 0xFF, (color >> 8) & 0xFF, (color) & 0xFF, (color >> 24) & 0xFF);
	SDL_RenderFillRect(this->renderer, &r);
}

static void g_drawRect(gfx_t * this, int x, int y, int w, int h, uint32_t color)
{
	SDL_Rect r = {camx(this, x), camy(this, y), w, h};
	SDL_SetRenderDrawColor(this->renderer,  (color >> 16) & 0xFF, (color >> 8) & 0xFF, (color) & 0xFF, (color >> 24) & 0xFF);
	SDL_RenderDrawRect(this->renderer, &r);
}

static void g_drawLine(gfx_t * this, int x0, int y0, int x1, int y1, uint32_t color) 
{
	SDL_SetRenderDrawColor(this->renderer,  (color >> 16) & 0xFF, (color >> 8) & 0xFF, (color) & 0xFF, (color >> 24) & 0xFF);
	SDL_RenderDrawLine(this->renderer, camx(this, x0), camy(this, y0), camx(this, x1), camy(this, y1));
}

static void g_render(gfx_t * this)
{
	//SDL_UpdateWindowSurface(window);
	SDL_RenderPresent(this->renderer);
}

static double g_time(gfx_t * this) 
{
	return (double) SDL_GetPerformanceCounter()/(double)SDL_GetPerformanceFrequency();
}

static void g_run(gfx_t * this, ctrl_t * root)
{
	bool done = false;
	double freq = (double)SDL_GetPerformanceFrequency();
	double dt = 1 / 60.0;
	//uint64_t last = 0;
	int mx=0;
	int my=0;
	rect_t rect = {0,0,this->gw,this->gh};
	ctrl_setrect(root, &rect);
	while (!done)
	{
		SDL_Event event;
		uint64_t start = SDL_GetPerformanceCounter();
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				done = true;
				break;
			case SDL_KEYDOWN:
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					done = true;
				}
				ctrl_keydown(root, mx, my, event.key.keysym.sym);
				break;
			case SDL_KEYUP:	
				ctrl_keyup(root, mx, my, event.key.keysym.sym);
				break;
			case SDL_MOUSEMOTION:
				mx = icamx(this, event.motion.x);
				my = icamy(this, event.motion.y);
				ctrl_mousemotion(root, mx, my);
				break;
			case SDL_MOUSEBUTTONDOWN:
				mx = icamx(this, event.motion.x);
				my = icamy(this, event.motion.y);
				ctrl_mousebuttondown(root, mx, my, event.button.button);
				break;
			case SDL_MOUSEBUTTONUP:
				mx = icamx(this, event.motion.x);
				my = icamy(this, event.motion.y);
				ctrl_mousebuttonup(root, mx, my, event.button.button);
				break;
			case SDL_WINDOWEVENT:
				if(event.window.event == SDL_WINDOWEVENT_RESIZED) {
					rect_t rect = {0,0,event.window.data1, event.window.data2};
					this->gw = event.window.data1;
					this->gh = event.window.data2;
					ctrl_setrect(root, &rect);
				}
				break;
			}
		}
		gfx_clear(this, 0xFFAAAAAA);
		ctrl_update(root, this);
		
		while(bdqueue_size(this->queue)) {
			cmd_t cmd;
			size_t n = bdqueue_dequeue(this->queue, &cmd, sizeof(cmd_t));
			if(n == sizeof(cmd_t)) {
				cmd.cmd(cmd.arg);
			}
		}
		gfx_render(this);
		uint64_t done = SDL_GetPerformanceCounter();
		dt = (done - start) / freq;
		if (dt < 1.0 / 60.0)
		{
			SDL_Delay(1000.0 * (1.0 / 60.0 - dt));
		}
		//uint64_t now = SDL_GetPerformanceCounter();
		//actualdt = (now - last) / freq;
		//last = now;
	}
}


static int g_w(gfx_t * this) { return this->gw; }
static int g_h(gfx_t * this) { return this->gh; }

static void g_close(gfx_t * this)
{
	//SDL_FreeSurface(screenSurface);
	bdqueue_free(this->queue);
	TTF_CloseFont(this->font);
	TTF_Quit();
	SDL_DestroyWindow(this->window);
	SDL_Quit();
}

static void g_drawText(gfx_t * this, const char * str, int x, int y, labelCenter_t center, uint32_t color) 
{
	SDL_Color textColor = { (color >> 16) & 0xFF, (color >> 8) & 0xFF, (color)&0xFF, (color>>24) & 0xFF};
	SDL_Surface* surfaceMessage = TTF_RenderText_Solid(this->font, str, textColor); // as TTF_RenderText_Solid could only be used on SDL_Surface then you have to create the surface first

	SDL_Texture* Message = SDL_CreateTextureFromSurface(this->renderer, surfaceMessage); //now you can convert it into a texture

	SDL_Rect Message_rect; //create a rect
	
	TTF_SizeText(this->font, str, &Message_rect.w, &Message_rect.h);
	if(center & CENTER_W) {
		Message_rect.x = x - Message_rect.w / 2;  //controls the rect's x coordinate 
	} else {
		Message_rect.x = x;  //controls the rect's x coordinate 
	}
	if(center & CENTER_H) {
		Message_rect.y = y - Message_rect.h / 2; // controls the rect's y coordinte
	} else {
		Message_rect.y = y; // controls the rect's y coordinte
	}
	//Mind you that (0,0) is on the top left of the window/screen, think a rect as the text's box, that way it would be very simple to understand

	//Now since it's a texture, you have to put RenderCopy in your game loop area, the area where the whole code executes

	SDL_RenderCopy(this->renderer, Message, NULL, &Message_rect); //you put the renderer's name first, the Message, the crop size(you can ignore this if you don't want to dabble with cropping), and the rect which is the size and coordinate of your texture

	//Don't forget to free your surface and texture
	SDL_FreeSurface(surfaceMessage);
	SDL_DestroyTexture(Message);
}


void g_beginInvoke(gfx_t * this, void(*cmd_fn)(void *arg), void * arg) 
{
	cmd_t cmd = {cmd_fn, arg};
	
	bdqueue_enqueue(this->queue, &cmd, sizeof(cmd_t));
}

static gfx_vtable_t vtable=
{
	g_clear,
	g_drawRect,
	g_fillRect,
	g_drawLine,
	g_render,
	g_time,
	g_run,
	g_w,
	g_h,
	g_close,
	g_drawText,
	g_beginInvoke,
};

gfx_t * sdlgfx_init(int w, int h)
{
	gfx_t * this = malloc(sizeof(gfx_t));
	if(this) {
		this->vtable = &vtable;
		this->gw = w;
		this->gh = h;
		if (SDL_Init(SDL_INIT_VIDEO) < 0)
		{
			fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());
			return false;
		}
		if(TTF_Init() < 0) {
			fprintf(stderr, "could not initialize sdl2_ttf: %s\n", TTF_GetError());
			return false;
		}
		this->window = SDL_CreateWindow(
		"hello_sdl2",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		w, h,
		SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE);
		if (this->window == NULL)
		{
			fprintf(stderr, "could not create window: %s\n", SDL_GetError());
			return false;
		}
		//screenSurface = SDL_GetWindowSurface(window);
		this->renderer = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);
		this->font = TTF_OpenFont("/home/kjell/git2/linforms/font.ttf", 16); //this opens a font style and sets a size
		if(this->font == NULL) {
			printf("font == NULL!\n");
		}
		this->queue = bdqueue_init();
		//this->track = NULL;
	}
	return this;
}
