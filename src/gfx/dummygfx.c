#include <stdlib.h>

#include "gfx.h"

struct gfx {
	gfx_vtable_t * vtable;
};


static void g_clear(gfx_t * this, uint32_t color)
{
}

static void g_fillRect(gfx_t * this, int x, int y, int w, int h, uint32_t color)
{
}

static void g_drawRect(gfx_t * this, int x, int y, int w, int h, uint32_t color)
{
}

static void g_drawLine(gfx_t * this, int x0, int y0, int x1, int y1, uint32_t color) 
{
}

static void g_render(gfx_t * this)
{
}

static double g_time(gfx_t * this) 
{
	return 0.0;
}

static void g_run(gfx_t * this, ctrl_t * root)
{
}


static int g_w(gfx_t * this) { return 0; }
static int g_h(gfx_t * this) { return 0; }

static void g_close(gfx_t * this)
{
	free(this);
}

static void g_drawText(gfx_t * this, const char * str, int x, int y, labelCenter_t center, uint32_t color) 
{

}

static gfx_vtable_t vtable=
{
	g_clear,
	g_drawRect,
	g_fillRect,
	g_drawLine,
	g_render,
	g_time,
	g_run,
	g_w,
	g_h,
	g_close,
	g_drawText
};

gfx_t * dummygfx_init(int w, int h)
{
	gfx_t * this = malloc(sizeof(gfx_t));
	if(this){
		this->vtable = &vtable;
	}
	return this;
}