#include "gfx.h"

struct gfx
{
	gfx_vtable_t *vtable;
};

void gfx_clear(gfx_t *this, uint32_t color)
{
	this->vtable->clear(this, color);
}
void gfx_drawRect(gfx_t *this, int x, int y, int w, int h, uint32_t color)
{
	this->vtable->drawRect(this, x, y, w, h, color);
}

void gfx_fillRect(gfx_t *this, int x, int y, int w, int h, uint32_t color)
{
	this->vtable->fillRect(this, x, y, w, h, color);
}

void gfx_drawLine(gfx_t *this, int x0, int y0, int x1, int y1, uint32_t color)
{
	this->vtable->drawLine(this, x0, y0, x1, y1, color);
}

void gfx_render(gfx_t *this)
{
	this->vtable->render(this);
}

double gfx_time(gfx_t *this)
{
	return this->vtable->time(this);
}

void gfx_run(gfx_t *this, ctrl_t *root)
{
	this->vtable->run(this, root);
}

int gfx_w(gfx_t *this)
{
	return this->vtable->w(this);
}

int gfx_h(gfx_t *this)
{
	return this->vtable->h(this);
}

void gfx_close(gfx_t *this)
{
	this->vtable->close(this);
}

void gfx_drawText(gfx_t * this, const char * str, int x, int y, labelCenter_t center, uint32_t color) 
{
	this->vtable->drawText(this, str, x, y, center, color);
}

void gfx_beginInvoke(gfx_t * this, void (*cmd)(void*arg), void*arg) 
{
	this->vtable->beginInvoke(this, cmd, arg);
}