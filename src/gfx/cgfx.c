#include "gfx.h"

void cgfx_frame(gfx_t * this, int x, int y, int w, int h, int state) 
{
	if(state == 0) {
		gfx_drawRect(this, x,y,w,h,0x00);
	} else if(state < 0) {
		gfx_drawLine(this, x,y,x+w,y, 0x0);
		gfx_drawLine(this, x,y,x,y+h, 0x0);
		gfx_drawLine(this, x+w,y,x+w,y+h, 0xFFFFFF);
		gfx_drawLine(this, x,y+h,x+w,y+h, 0xFFFFFF);
	} else if(state > 0) {
		gfx_drawLine(this, x+w,y,x+w,y+h, 0x0);
		gfx_drawLine(this, x,y+h,x+w,y+h, 0x0);
		gfx_drawLine(this, x,y,x+w,y, 0xFFFFFF);
		gfx_drawLine(this, x,y,x,y+h, 0xFFFFFF);
	}
}