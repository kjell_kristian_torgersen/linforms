#ifndef INC_GFX_CGFX
#define INC_GFX_CGFX

typedef struct gfx gfx_t;

void cgfx_frame(gfx_t * this, int x, int y, int w, int h, int state);

#endif /* INC_GFX_CGFX */
