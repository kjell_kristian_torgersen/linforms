#ifndef __GFX_H__
#define __GFX_H__

#include <stdint.h>
#include <stdbool.h>

#include "ctrl.h"
//#include "ent.h"
typedef enum labelCenter {
	CENTER_NONE, CENTER_W, CENTER_H, CENTER_WH
} labelCenter_t;

typedef struct gfx gfx_t;

typedef struct cmd cmd_t;

typedef struct cmd_vtable {
	void(*execute)(cmd_t * this);
	void(*free)(cmd_t * this);
} cmd_vtable_t;

typedef struct gfx_vtable {
	void (*clear)(gfx_t * this, uint32_t color);
	void (*drawRect)(gfx_t * this, int x, int y, int w, int h, uint32_t color);
	void (*fillRect)(gfx_t * this, int x, int y, int w, int h, uint32_t color);
	void (*drawLine)(gfx_t * this, int x0, int y0, int x1, int y1, uint32_t color);
	void (*render)(gfx_t * this);
	double (*time)(gfx_t * this);
	void (*run)(gfx_t * this, ctrl_t * root);
	int (*w)(gfx_t * this);
	int (*h)(gfx_t * this);
	void (*close)(gfx_t * this);
	void (*drawText)(gfx_t * this, const char * str, int x, int y, labelCenter_t center, uint32_t color);
	void (*beginInvoke)(gfx_t * this, void (*cmd)(void*arg), void*arg);
} gfx_vtable_t;

void gfx_clear(gfx_t * this, uint32_t color);
void gfx_drawRect(gfx_t * this, int x, int y, int w, int h, uint32_t color);
void gfx_fillRect(gfx_t * this, int x, int y, int w, int h, uint32_t color);
void gfx_drawLine(gfx_t * this, int x0, int y0, int x1, int y1, uint32_t color);
void gfx_render(gfx_t * this);
double gfx_time(gfx_t * this);
void gfx_run(gfx_t * this, ctrl_t * root);
int gfx_w(gfx_t * this);
int gfx_h(gfx_t * this);
void gfx_close(gfx_t * this);
void gfx_drawText(gfx_t * this, const char * str, int x, int y, labelCenter_t center, uint32_t color);
void gfx_beginInvoke(gfx_t * this, void (*cmd)(void*arg), void*arg);

gfx_t * sdlgfx_init(int w, int h);
gfx_t * dummygfx_init(int w, int h);

#endif // !__GFX_H__
