#ifndef CTRL_H
#define CTRL_H

#include <stdbool.h>
#include <stdint.h>

typedef struct ctrl ctrl_t;
typedef struct ctrl_common ctrl_common_t;
typedef struct gfx gfx_t;

typedef enum ctrl_type {
	CTRL_CUSTOM, CTRL_BUTTON, CTRL_HCONTAINER, CTRL_VCONTAINER, CTRL_LABEL, CTRL_PROGRESSBAR, CTRL_TEXTEDIT
} ctrl_type_t;

typedef struct rect {
	int x;
	int y;
	int w;
	int h;
} rect_t;

typedef struct ctrl_vtable {
	void(*setrect)(ctrl_t * this, const rect_t * rect);
	void(*getrect)(ctrl_t * this, rect_t * rect);
	void(*keydown)(ctrl_t * this, int x, int y, int key);
	void(*keyup)(ctrl_t * this, int x, int y, int key);
	void(*mousemotion)(ctrl_t * this, int x, int y);
	void(*mousebuttondown)(ctrl_t * this, int x, int y, int button);
	void(*mousebuttonup)(ctrl_t * this, int x, int y, int button);
	void(*update)(ctrl_t * this, gfx_t * gfx);
	void(*free)(ctrl_t * this);
} ctrl_vtable_t;

typedef struct ctrl_event {
	void(*mousebuttondown)(ctrl_t * this, int x, int y, int button);
	void(*mousebuttonup)(ctrl_t * this, int x, int y, int button);
	void(*mousemotion)(ctrl_t * this, int x, int y);
	void(*keydown)(ctrl_t * this, int x, int y, int key);
	void(*keyup)(ctrl_t * this, int x, int y, int key);
} ctrl_event_t;

struct ctrl_common {
	ctrl_vtable_t * vtable;
	ctrl_event_t events;
	void * arg;
	rect_t rect;
	int weight;
	ctrl_type_t type;
	uint32_t foreColor;
	uint32_t backColor;
};

bool ctrl_inside(ctrl_t * this, int x, int y);
void ctrl_setEvents(ctrl_t * this, ctrl_event_t * events, void * arg);
void ctrl_common_init(ctrl_common_t * this, ctrl_vtable_t * vtable, ctrl_type_t type);
void ctrl_setrect(ctrl_t * this, const rect_t * rect);
void ctrl_getrect(ctrl_t * this, rect_t * rect);
void ctrl_keydown(ctrl_t * this, int x, int y, int key);
void ctrl_keyup(ctrl_t * this, int x, int y, int key);
void ctrl_mousemotion(ctrl_t * this, int x, int y);
void ctrl_mousebuttondown(ctrl_t * this, int x, int y, int button);
void ctrl_mousebuttonup(ctrl_t * this, int x, int y, int button);
void ctrl_update(ctrl_t * this, gfx_t * gfx);
void ctrl_free(ctrl_t * this);

ctrl_t * ctrl_container_init(bool horizontal);
ctrl_t * ctrl_label_init(const char * text);
ctrl_t * ctrl_button_init(const char * text);
void ctrl_container_append(ctrl_t * this, ctrl_t * ctrl);
void ctrl_container_append(ctrl_t * this, ctrl_t * ctrl);
void ctrl_label_setText(ctrl_t * this, const char * text);
void ctrl_textedit_appendText(ctrl_t * this, const char * str);
const char * ctrl_button_text(ctrl_t * this);
ctrl_t * ctrl_textedit_init(void);
void ctrl_textedit_clear(ctrl_t * this);
void ctrl_textedit_appendText(ctrl_t * this, const char * str);
ctrl_t * ctrl_progressBar_init(void);
void ctrl_progressBar_setProgress(ctrl_t * this, int progress);
void ctrl_progressBar_setProgressMax(ctrl_t * this, int progressMax);
ctrl_t * ctrl_checkbox_init(const char * text);
void ctrl_tabs_addTab(ctrl_t * this, const char * name, ctrl_t * content);
ctrl_t * ctrl_tabs_init(void) ;

#endif /* CTRL_H */
